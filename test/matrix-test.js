'use strict'

import test from 'ava'
import Matrix from '../libs/matrix'

test('create a matrix', async t => {
  const matrix = new Matrix()
  t.is(typeof matrix.Create, 'function', 'Matrix.create should be a function')
  let trix = [
    [
      [0, 0],
      [0, 0]
    ],
    [
      [0, 0],
      [0, 0]
    ]
  ]

  let result = matrix.Create(2)
  t.deepEqual(result, trix)
})

test('update matrix', async t => {
  const matrix = new Matrix()
  t.is(typeof matrix.Update, 'function', 'Matrix.update should be a function')
  let arr = ['UPDATE', '2', '2', '2', '4']
  let expectedMatrix = [
    [
      [0, 0],
      [0, 0]
    ],
    [
      [0, 0],
      [0, 4]
    ]
  ]
  let newMatrix = matrix.Create(2)
  let result = matrix.Update(arr, newMatrix)

  t.deepEqual(result, expectedMatrix)
})

test('query a matrix', async t => {
  const matrix = new Matrix()
  t.is(typeof matrix.Query, 'function', 'Matrix.query should be a function')
  let arr = ['QUERY', '1', '1', '1', '3', '3', '3']

  let newMatrix = matrix.Create(5)
  let result = matrix.Query(arr, newMatrix)

  t.is(result, 0)
})

test('throw error on a matrix with invalid index', async t => {
  const matrix = new Matrix()
  let arr = ['QUERY', '1', '1', '1', '3', '3', '3']

  const error = t.throws(() => {
    let newMatrix = matrix.Create(2)
    matrix.Query(arr, newMatrix)
  }, Error)

  t.is(error.message, 'Cannot read property \'0\' of undefined')
})

test('run update instruction on matrix', async t => {
  const matrix = new Matrix()
  t.is(typeof matrix.GetInstruction, 'function', 'Matrix.getInstruction should be a function')
  let sentence = 'UPDATE 2 2 2 4'
  sentence = sentence.split(' ')
  let trix = []

  let result = matrix.GetInstruction(sentence[0], trix)

  t.is(result, 0)
})

test('run query instruction on matrix', async t => {
  const matrix = new Matrix()
  t.is(typeof matrix.GetInstruction, 'function', 'Matrix.getInstruction should be a function')
  let sentence = 'QUERY 2 2 2 4 4 4'
  sentence = sentence.split(' ')
  let trix = []

  let result = matrix.GetInstruction(sentence[0], trix)

  t.is(result, 1)
})

test('run invalid instruction on matrix', async t => {
  const matrix = new Matrix()
  t.is(typeof matrix.GetInstruction, 'function', 'Matrix.getInstruction should be a function')
  let sentence = 'INSERT 2 2 2 4'
  sentence = sentence.split(' ')
  let trix = []

  const error = t.throws(() => {
    matrix.GetInstruction(sentence[0], trix)
  }, Error)

  t.is(error.message, 'Instruction is not valid.')
})
