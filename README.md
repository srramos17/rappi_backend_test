# Rappi Backend Test

## Code Challenge

Cube summation test solution.

## Capas
1. Persistencia: Está conformada por el archivo ```server.js```.
    El archivo ```server.js``` está conformado por las siguientes secciones:
    - Los requires de los módulos que se implementan para este servidor.
    - Se establece el motor de vistas de la aplicación a HTML.
    - Se configuran los middleware del server. A saber:
      * Parseo de datos del request a json -bodyParse.json()-.
      * Se sirven los archivos estáticos en la ruta public.
      * COnfiguración del motor de vistas a HTML.
      * Helmet para seguridad del server eliminando headers innecesarios,
    - Se establece la ruta raiz ```'/'``` la cual sirve el archivo index.html. No se especifica la extensión del archivo debido a que, previamente se configuro el motor de vistas a archivos HTML.
    - Se expone un servicio ```POST '/process'``` para procesar los datos que se obtienen del request.
    - Se consumen los servicios expuestos por el cliente de 'Matrix'.
    - El cliente ```matrix.js``` expone los servicios de interacción y manipulación de la matrix. Con estos servicios se completa la capa de persistencia puesto que mediante estos servicios se logra transformar los datos que son enviados por el usuario.
    - Para el caso que no se use la aplicación web, el archivo ```index.js``` es quien sirve de capa de persistencia. Se invoca desde la consola y obtiene los datos del archivo ```data.js```.

        ### Uso
        Instalar dependencias e invocar el servicio.
        ```sh
          $ npm install
          $ npm run index
        ```

2. Aplicación: Compuesta por los módulos de ```angularJS```. Cada módulo cuenta con un componente, podria tener varios componentes si fuera necesario, el cual se gestiona mediante el controlador quien es el que maneja la lógica de la aplicación y envia a la vista los datos obtenidos de la capa de persistencia.
    La comunicación a la capa de persistencia se realiza mediante peticiones HTTP usando el servicio $http que expone angularJS a las rutas expuestas del server. Dentro de esta capa se encuentra el archivo ```main.module.js``` que depende directamente del archivo ```app.js``` que contiene la configuración de la capa de vistas y aplicación.

3. Vista:  AngularJs es quien se encarga de renderizar las vistas con los datos que le suministra el controlador del componente.
    El archivo index.html es quien sirve de contenedor para renderizar los componentes de la aplicación. Para este caso, se renderiza un único componente que es main.module.js a traves del template main.template.html.
    La configuración de las rutas, para indicar que componente se debe renderizar, la tiene al archivo app.js. Se importan los componentes a usar, ya que se trabaja mediante una arquitectura basada en componentes, y se implementan en las rutas.

## Responsabilidades

1. Backend:
  - ``` server.js ```: Inicializa una instancia del servidor HTTP que contendrá la aplicación. Expone rutas con diferentes métodos HTTP.
  - ``` index.js ```: A diferencia del ```server.js```, se invoca desde la consola con el script ```npm run index``` y carece de interfaz web. Consume los métodos expuestos de la clase ```Matrix``` y obtiene el set de datos del archivo data.js (que pueden ser cambiados)
  - ``` libs/matrix.js```: Clase que contiene y expone los métodos de servicio para crear, insertar y actualizar los datos de la matriz.
  - ```data.js```: Contiene un conjunto de datos de muestra que son consumidos por el archivo ```index.js```.

2. Frontend:
  - ```app.js```: Contiene la configuración de las vistas. Es AngularJs quien soporta la capa del front. En este archivo se especifican las dependencias de otros compontes, las rutas de nuestra aplicación -para saber en que ruta se debe renderizar cada componente- y entre otras configuraciones según la magnitud del proyecto.
  - ```components/main.module.js```: Compone un módulo de la aplicación. Contiene la lógica de la aplicación y obtiene los datos mediante peticiones HTTP. Se enfoca en enviar los datos del usuario y en obtener el resultado y finalmente enviar los datos a la vista para que sea mostrada. Se debe agregar como dependencia al archivo principal de configuración.
  - ```components/main.template.html```: Contiene la vista de la aplicación. Renderiza y muestra los datos que recibe del controlador del componente.

3. Otros:
  - ```test/matrix-test.js```: Contiene el conjunto de pruebas unitarias del servicio de matrix.js con la libreria ava.

## Instalación
  1. Instala las dependencias.
  2. Ejecuta las pruebas unitarias
  3. Inicia el servidor.
  4. Navega a http://localhost:3000

  ```sh
  $ cd cubeSummation
  $ npm install -d
  $ npm test
  $ npm start
  ```