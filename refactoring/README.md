# Rappi Backend Test

## Code Refactoring

Las malas prácticas evidenciadas en el código son:
1. Código comentariado. EL codigo se debe escribir de tal manera que pueda ser entendido sin necesidad de comentarse. Pero no es el caso del ejemplo ya que, se encuentra código sin usar comentado. Con los sistemas de versionamiento esta mala práctica se debe eliminar.

2. No declarar variables con información de uso frecuente. Se tiene un parametro que se obtiene del request y se llama cada vez que se va a usar. es una mala practica porque podemos reducir el trabajo de procesamieto y aumentar la velocidad de la aplicación si asignamos a una variable el valor constante que se usa con frecuencia.

3. Para el caso de javascript, es una mala práctica asignar variables o constantes con la palabra reservada ```var``` ya que al final todas quedan definidas en el scope global. Para superar esta mala práctica se debe hacer uso de la palabra reservada ```const``` para asignar valores constantes ya que este tipo de dato es inmutable. Por otro lado se puede usar la palabra reservada ```let``` para asignar valores que pueden cambiar. De esta manera se hace un mejor uso de los recursos de las máquinas y mejora la calidad del código.

4. Es una mala práctica realizar llamados de servicios indiscriminadamente. Se debe tener claro el flujo del proceso para realizar la menor cantidad de llamadas de un servicio, sobre todo si van a modificar los datos de una BD, para lograr el mismo resultado.

5. Mala práctica declarar variables que no se van a usar. Consumen recursos innecesariamente.

6. Se puede evitar el uso del ```if-else```. Para ello, se valida la condicion opuesta al escenario ideal y en caso de ser verdadera se genera inmediatamente el return para terminar el hilo de ejecución. De esta forma para todas las condiciones falsas que se tengan y finalmente sin el uso del ```else``` el método queda con las lineas del escenario ideal.

7. Repetir código. Siguiendo el estandar DRY (Dont Repeat Yourself), se debe reutilizar lineas de codigo. Para lograrlo, se puede hacer uso de métodos mas específicos, se puede apoyar en la creacion de objetos repetitivos que se instancian una sola vez, pero se invocan multiples veces.

8. Como buena práctica se debe tener la declaración de variables al inicio de los métodos  y agrupados. No desordenados a lo largo de un método.

9. El manejo de excepciones es fundamental en segmentos donde no sabemos que errores se pueden presentar. Para ellos se hace uso de la sentencia ```try-catch```. Nos permite capturar excepciones para darles un manejo o para relanzarlas y que sean manejadas en otra instancia de la aplicación.

10. Código espaguetti. El codigo debe ser legible para cualquier persona. Debe ser descriptivo en el nombramiento de sus funciones y variables. Es una buena práctica dejar un espacio despues de bloques de código como los bloques if y sus semejantes.

La refactorización realizada, supera estas malas prácticas de escritura de codigo basado en algunos patrones. Patrones de nombramiento de variables y métodos con camelCase, por ejemplo, y nombres que describan la función o el propósito del mismo. Esto con el fin de no recurrir a la documentación innecesaria.
Se reducen las lineas de código lo que genera métodos más pequeños y especificos.
Se implementan principios SOLID como el de resposabilidad simple, y open closed. Igualmente patrones de diseño creacionales como el de Singleton que garantiza la existencia de una sola instancia para una clase.


## Preguntas

1. ¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito?
    Es uno de los cinco principios fundamentales que conforman los principios SOLID de diseño de software. Básicamente resume que cada clase debe tener una sola funcionalidad o finalidad. Dicho de otra forma, es lo opueso a tener la, llamada popular, clase de utilidades en donde, se encuentran métodos que hacen de todo.
    Busca dar a cada clase una sola responsabilidad o propósito dentro de un sistema.

2. ¿Qué características tiene según tu opinión “buen” código o código limpio?
    - El buen código debe ser un codigo legible. Debe carecer totalmente de declaraciones de variables y métodos con la notación húngara. y al igual que en un libro, debe tener sus secciones bien definidas.
    - Debe tener métodos que realizan cosas especificas y no el super método de mil lineas en donde se hacen todo tipo de operaciones.
    - Debe hacer uso de las últimas caracteristicas liberadas ya que, garantizan un manejo eficiente de los recursos.
    - Debe ser declarativo siguiendo siempre un mismo estándar y carecer de comentarios que no sean justificados.
    - Debe seguir un patrón. Para el caso de JavaScript, el patrón propuesto y acogido por la mayoria de la comunidad se resume en un linter llamado 'standard'. Éste tiene, y valida, las buenas prácticas a la hora de escribir código.

