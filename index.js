'use strict'

import Debug from 'debug'
import Matrix from './libs/matrix'
import { sampleData } from './data'

const matrix = new Matrix()
const debug = new Debug('CubeSummation:Console:index')

function process () {
  let current = 0
  let result = []

  let steps = parseInt(sampleData[current++])

  try {
    for (let step = 0; step < steps; step++) {
      let secondIndex = sampleData[current++]
      .split(' ')
      .map((n) => parseInt(n))

      let sizeArray = secondIndex[0]
      let attempts = secondIndex[1]
      let trix = matrix.Create(sizeArray)

      for (let attemp = 0; attemp < attempts; attemp++) {
        let sentence = sampleData[current++]
        let array = sentence.split(' ')
        let query = matrix.GetInstruction(array[0])

        if (query) {
          let ret = matrix.Query(array, trix)
          result.push(ret)
        } else {
          matrix.Update(array, trix)
        }
      }
    }

    debug(result.join('\n'))
  } catch (e) {
    debug(e.message)
  }
}

process()
